//
//  CameraViewModel.swift
//  MLVisionExample
//
//  Created by Rafael Couto Estrela on 09/09/19.
//  Copyright © 2019 Google Inc. All rights reserved.
//

import UIKit

// MARK: definitions

fileprivate struct Improvement {
    static let more: String = "Um pouco mais..."
    static let singleClosed: String = "Feche apenas o olho solicitado..."
    static let changeSide: String = "Para o outro lado..."
    static let keepStraight: String = "Fique olhando para a câmera..."
    static let singleFace: String = "Mantenha apenas um rosto na câmera..."
}

fileprivate enum LivenessOperationTrust {
    case any
    case low
    case high
}

enum LivenessOperation {
    case blinkLeft
    case blinkRight
    case smile
    case plain
    case lookLeft
    case lookRight
    case tiltLeft
    case tiltRight
    
    fileprivate var trustLevel: LivenessOperationTrust {
        switch self {
        case .blinkLeft, .blinkRight, .smile: return .high
        default: return .low
        }
    }
    
    fileprivate func random(
        history: [LivenessOperation],
        desired trust: LivenessOperationTrust
    ) -> LivenessOperation {
        let index = Int.random(in: 0...6)
        
        let randomized: LivenessOperation
        
        switch index {
        case 0: randomized = .smile
        case 1: randomized = .blinkLeft
        case 2: randomized = .blinkRight
        case 3: randomized = .lookLeft
        case 4: randomized = .lookRight
        case 5: randomized = .tiltLeft
        case 6: randomized = .tiltRight
        default: randomized = .plain
        }
        
        if trust == .any || trust == randomized.trustLevel && !history.contains(randomized) {
            return randomized
        }
        
        return self.random(history: history, desired: trust)
    }
    
    fileprivate var operationLabel: String {
        switch self {
        case .blinkLeft: return "Feche o olho esquerdo."
        case .blinkRight: return "Feche o olho direito."
        case .smile: return "Sorria."
        case .plain: return "Olhe para a câmera."
        case .lookLeft: return "Olhe para a esquerda."
        case .lookRight: return "Olhe para a direita."
        case .tiltLeft: return "Incline a cabeça para a esquerda."
        case .tiltRight: return "Incline a cabeça para a direita."
        }
    }
}

protocol CameraDelegate {
    func improve(_ label: String?)
    func operationDidChange(_ operation: String)
    func operationDidComplete()
}

// MARK: class

class CameraViewModel: NSObject {
    var shouldStart: Bool = false
    
    private var operationHistory: [LivenessOperation] = []
    
    private var currentOperation: LivenessOperation = .plain {
        didSet {
            self.delegate.operationDidChange(self.currentOperation.operationLabel)
            
            self.operationHistory.append(self.currentOperation)
        }
    }
    
    private var isCapturing: Bool = false
    private let delegate: CameraDelegate
    
    private var timer: Timer?
    
    private var lowTrustOperations: Int {
        return self.operationHistory.filter { $0.trustLevel == .low }.count
    }
    
    private var highTrustOperations: Int {
        return self.operationHistory.filter { $0.trustLevel == .high }.count
    }
    
    private var desiredTrust: LivenessOperationTrust {
        if lowTrustOperations == 2 {
            return .high
        } else if highTrustOperations == 1 {
            return .low
        }
        
        return .any
    }
    
    private var numberOfOperations: Int {
        return self.lowTrustOperations + self.highTrustOperations
    }
    
    init(delegate: CameraDelegate) {
        self.delegate = delegate
        
        //delegate.operationDidChange(self.currentOperation.operationLabel)
    }
    
    func reset() {
        self.currentOperation = .plain
        self.operationHistory.removeAll()
        
        self.timer = nil
        self.isCapturing = false
        self.shouldStart = false
    }
    
    func prepareForDetection() {
        if self.timer == nil {
            DispatchQueue.main.async {
                self.timer = Timer.scheduledTimer(
                    timeInterval: 1.5, target: self,
                    selector: #selector(self.startRecognizing),
                    userInfo: nil, repeats: false
                )
            }
        }
    }
    
    func tooManyFacesDetected() {
        self.delegate.improve(Improvement.singleFace)
    }
    
    // TILT RIGHT -> NEGATIVE ROLL
    // TILT LEFT -> POSITIVE ROLL
    // LOOK LEFT -> NEGATIVE YAW
    // LOOK RIGHT -> POSITIVE YAW
    // ALL -> -90 to 90
    func processOperationTrial(
        roll: CGFloat, yaw: CGFloat,
        smileProbability: CGFloat,
        leftEyeOpenProbability: CGFloat, rightEyeOpenProbability: CGFloat
    ) {
        if self.isCapturing { return }
        
        self.isCapturing = true
        
        var success = false
        
        let isPlainRoll = (-10.0 ... 10.0).contains(roll)
        let isPlainYaw = (-10.0 ... 10.0).contains(yaw)
        
        let isPlain = isPlainRoll && isPlainYaw
        
        self.checkIfNeedsImprovement(
            roll: roll, yaw: yaw,
            smileProbability: smileProbability,
            leftEyeOpenProbability: leftEyeOpenProbability,
            rightEyeOpenProbability: rightEyeOpenProbability
        )
        
        switch self.currentOperation {
        case .smile://yaw == 0; roll == 0; face.hasSmile
            success = isPlain && smileProbability >= 0.9
        case .blinkLeft://yaw == 0; roll == 0; face.leftEyeClosed
            success = isPlain && leftEyeOpenProbability < 0.3 && rightEyeOpenProbability >= 0.7
        case .blinkRight://yaw == 0; roll == 0; face.rightEyeClosed
            success = isPlain && leftEyeOpenProbability >= 0.7 && rightEyeOpenProbability < 0.3
        case .lookLeft://0<=yaw<=0.5; roll == 0
            success = isPlainRoll && (-90.0 ... (-30.0)).contains(yaw)
        case .lookRight://-0.5<=yaw<=0; roll == 0
            success = isPlainRoll && (30.0 ... 90.0).contains(yaw)
        case .tiltLeft://yaw == 0; -0.5<=roll<=0
            success = isPlainYaw && (20.0 ... 90.0).contains(roll)
        case .tiltRight://yaw == 0; 0<=roll<=0.5
            success = isPlainYaw && (-90.0 ... (-20.0)).contains(roll)
        case .plain://yaw == 0; roll == 0
            success = isPlain
        }
        
        if success {
            if self.numberOfOperations == 3 {
                self.delegate.operationDidComplete()
            } else {
                self.operationSuccess()
            }
        } else {
            self.isCapturing = false
        }
    }
    
    private func operationSuccess() {
        self.timer = nil
        self.isCapturing = false
        self.shouldStart = false
        
        self.currentOperation = self.currentOperation.random(
            history: self.operationHistory,
            desired: self.desiredTrust
        )
    }
    
    private func checkIfNeedsImprovement(
        roll: CGFloat, yaw: CGFloat,
        smileProbability: CGFloat,
        leftEyeOpenProbability: CGFloat, rightEyeOpenProbability: CGFloat
    ) {
        let isPlainRoll = (-10.0 ... 10.0).contains(roll)
        let isPlainYaw = (-10.0 ... 10.0).contains(yaw)
        
        let isPlain = isPlainRoll && isPlainYaw
        
        var value: String? = nil
        
        switch self.currentOperation {
        case .blinkLeft:
            if (0.3 ..< 0.7).contains(leftEyeOpenProbability) {
                value = Improvement.more
            } else if (0.0 ... 0.3).contains(rightEyeOpenProbability) {
                value = Improvement.singleClosed
            } else if !isPlain {
                value = Improvement.keepStraight
            }
        case .blinkRight:
            if (0.3 ..< 0.7).contains(rightEyeOpenProbability) {
                value = Improvement.more
            } else if (0.0 ... 0.3).contains(leftEyeOpenProbability) {
                value = Improvement.singleClosed
            } else if !isPlain {
                value = Improvement.keepStraight
            }
        case .smile:
            if (0.5 ..< 0.9).contains(smileProbability) {
                value = Improvement.more
            } else if !isPlain {
                value = Improvement.keepStraight
            }
        case .plain:
            if !isPlain {
                value = Improvement.keepStraight
            }
        case .lookLeft:
            if (0.0 ... 90.0).contains(yaw) {
                value = Improvement.changeSide
            } else if (-29.9 ..< 0.0).contains(yaw) {
                value = Improvement.more
            }
        case .lookRight:
            if (-90.0 ..< 0.0).contains(yaw) {
                value = Improvement.changeSide
            } else if (0.0 ..< 30.0).contains(yaw) {
                value = Improvement.more
            }
        case .tiltLeft:
            if (-90.0 ..< 0.0).contains(roll) {
                value = Improvement.changeSide
            } else if (0.0 ..< 20.0).contains(roll) {
                value = Improvement.more
            }
        case .tiltRight:
            if (0.0 ... 9.0).contains(roll) {
                value = Improvement.changeSide
            } else if (-19.9 ..< 0.0).contains(roll) {
                value = Improvement.more
            }
        }
        
        self.delegate.improve(value)
    }
    
    @objc private func startRecognizing() {
        self.shouldStart = true
    }
}
